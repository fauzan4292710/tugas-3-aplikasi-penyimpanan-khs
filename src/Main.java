/* 
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
*/

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("=== Aplikasi Perekaman KHS ===");

        // Memasukkan data beberapa mahasiswa
        boolean tambahMahasiswa = true;
        while (tambahMahasiswa) {
            System.out.print("\nMasukkan Nama Mahasiswa: ");
            String nama = scanner.nextLine();
            System.out.print("Masukkan NIM Mahasiswa: ");
            String nim = scanner.nextLine();
            Mahasiswa mahasiswa = new Mahasiswa(nama, nim);
            DatabaseMahasiswa.tambahMahasiswa(mahasiswa);

            // Memasukkan data mata kuliah untuk setiap mahasiswa
            System.out.println("\nData KHS untuk Mahasiswa dengan Nama: " + mahasiswa.getNama());
            KHS khs = new KHS(mahasiswa);
            boolean tambahMataKuliah = true;
            while (tambahMataKuliah) {
                System.out.println("\nMasukkan data mata kuliah:");
                System.out.print("Kode MK: ");
                String kode = scanner.nextLine();
                System.out.print("Nama MK: ");
                String namaMK = scanner.nextLine();
                System.out.print("Nilai Angka: ");
                double nilaiAngka = scanner.nextDouble();
                scanner.nextLine();

                MataKuliah mataKuliah = new MataKuliah(kode, namaMK, nilaiAngka);
                khs.tambahMataKuliah(mataKuliah);

                System.out.print("\nApakah Anda ingin menambah data mata kuliah lagi? (y/n): ");
                String pilihanMataKuliah = scanner.nextLine();
                tambahMataKuliah = (pilihanMataKuliah.equalsIgnoreCase("y"));
            }
            DatabaseKHS.tambahKHS(khs);

            System.out.print("\nApakah Anda ingin menambah mahasiswa lagi? (y/n): ");
            String pilihanMahasiswa = scanner.nextLine();
            tambahMahasiswa = (pilihanMahasiswa.equalsIgnoreCase("y"));
        }

        // Menampilkan KHS untuk setiap mahasiswa
        System.out.println("\n=== Kartu Hasil Studi (KHS) ===");
        for (KHS khs : DatabaseKHS.getKHSList()) {
            khs.cetakKHS();
            System.out.println();
        }

        scanner.close();
    }
}
