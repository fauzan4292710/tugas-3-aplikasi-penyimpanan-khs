/* 
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
*/

import java.util.ArrayList;
import java.util.List;

public class KHS {
    private Mahasiswa mahasiswa;
    private List<MataKuliah> mataKuliahList;

    public KHS(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
        this.mataKuliahList = new ArrayList<>();
    }

    public void tambahMataKuliah(MataKuliah mataKuliah) {
        mataKuliahList.add(mataKuliah);
    }

    public void cetakKHS() {
        System.out.println("\n=== Kartu Hasil Studi (KHS) untuk Mahasiswa ===");
        System.out.println("Nama: " + mahasiswa.getNama());
        System.out.println("NIM: " + mahasiswa.getNim());
        System.out.println("----------------------------------------------------------------");
        System.out.printf("| %-10s | %-20s | %-10s | %-8s |\n", "Kode MK", "Nama MK", "Nilai", "Nilai Huruf");
        System.out.println("----------------------------------------------------------------");
        for (MataKuliah mk : mataKuliahList) {
            System.out.printf("| %-10s | %-20s | %-10.2f | %-11s |\n", mk.getKode(), mk.getNamaMK(), mk.getNilaiAngka(),
                    mk.getNilaiHuruf());
        }
        System.out.println("----------------------------------------------------------------");
    }
}
