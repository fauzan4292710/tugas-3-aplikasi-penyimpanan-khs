/* 
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
*/

public class Mahasiswa {

    private String nama;
    private String nim;

    public Mahasiswa(String nama, String nim) {
        this.nama = nama;
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public String getNim() {
        return nim;
    }

}
