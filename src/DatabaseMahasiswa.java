/* 
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
*/

import java.util.ArrayList;
import java.util.List;

public class DatabaseMahasiswa {
    private static List<Mahasiswa> mahasiswaList = new ArrayList<>();

    public static void tambahMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaList.add(mahasiswa);
    }

    public static List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }
}
