/* 
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
*/

import java.util.ArrayList;
import java.util.List;

public class DatabaseKHS {
    private static List<KHS> khsList = new ArrayList<>();

    public static void tambahKHS(KHS khs) {
        khsList.add(khs);
    }

    public static List<KHS> getKHSList() {
        return khsList;
    }
}
