/* 
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
*/

public class MataKuliah {

    private String kode;
    private String namaMK;
    private double nilaiAngka;

    public MataKuliah(String kode, String namaMK, double nilaiAngka) {
        this.kode = kode;
        this.namaMK = namaMK;
        this.nilaiAngka = nilaiAngka;
    }

    public String getKode() {
        return kode;
    }

    public String getNamaMK() {
        return namaMK;
    }

    public double getNilaiAngka() {
        return nilaiAngka;
    }

    public String getNilaiHuruf() {
        if (nilaiAngka >= 80 && nilaiAngka <= 100) {
            return "A";
        } else if (nilaiAngka >= 70 && nilaiAngka < 80) {
            return "B";
        } else if (nilaiAngka >= 60 && nilaiAngka < 70) {
            return "C";
        } else if (nilaiAngka >= 50 && nilaiAngka < 60) {
            return "D";
        } else if (nilaiAngka >= 0 && nilaiAngka < 50) {
            return "E";
        } else {
            return "Nilai tidak valid!";
        }
    }

}
